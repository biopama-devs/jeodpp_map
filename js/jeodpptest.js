var tTipDelay = 100;
jQuery(document).ready(function($) {
	var mapPolyHostURL = "https://tiles.biopama.org/BIOPAMA_poly";
	var mapPaLayer = "WDPA2019MayPoly";
	//mapboxgl.accessToken = 'pk.eyJ1IjoiYmxpc2h0ZW4iLCJhIjoiMEZrNzFqRSJ9.0QBRA2HxTb8YHErUFRMPZg';
	mapboxgl.accessToken = 'pk.eyJ1IjoiamFtZXNkYXZ5IiwiYSI6ImNpenRuMmZ6OTAxMngzM25wNG81b2MwMTUifQ.A2YdXu17spFF-gl6yvHXaw';
	
	var map = new mapboxgl.Map({
		container: 'map-container',
		style: 'mapbox://styles/jamesdavy/cjw25laqe0y311dqulwkvnfoc', //Andrews default new RIS v2 style based on North Star
		attributionControl: true,
		renderWorldCopies: true,
		center: [0, -6.66],
        zoom: 1.5,
		minZoom: 1.4,
		maxZoom: 18
	}).addControl(new mapboxgl.AttributionControl({
        customAttribution: "JEODPP test.",
		compact: true
    }));
	map.on('load', function () {
		map.addSource("BIOPAMA_Poly", {
			"type": 'vector',
			"tiles": [mapPolyHostURL+"/{z}/{x}/{y}.pbf"],
			"minZoom": 0,
			"maxZoom": 18,
		});
		map.addLayer({
			"id": "wdpaAcpFill",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapPaLayer,
			"minzoom": 1,
			"paint": {
				"fill-color": [
					"match",
					["get", "MARINE"],
					["1"],
					"hsla(173, 21%, 51%, 0.2)",
					"hsla(87, 47%, 53%, 0.2)"
				],
				"fill-opacity": [
                    "interpolate",
                    ["exponential", 1],
                    ["zoom"],
                    3,
                    0.3,
                    5,
                    0.5,
                    6,
                    1
                ]
			}
		}, 'state-label-lg');
		map.addLayer({
			"id": "wdpaAcpHover",
			"type": "line",
			"source": "BIOPAMA_Poly",
			"minzoom": 4,
			"source-layer": mapPaLayer,
			"paint": {
				"line-color": "#8fc04f",
				"line-width": 1,
				"line-opacity": [
                    "interpolate",
                    ["exponential", 1],
                    ["zoom"],
                    3,
                    0.3,
                    5,
                    0.5,
                    6,
                    1
                ]
			}
		}, 'state-label-lg');

		$("#jeodpp-menu-container").css({ 'height': windowHeight + "px" });
		var jeodppOptions = {
			compare: true, //turn on compare function
			mapLayer: "wdpaAcpHover", //which layer will the sentinel map load under
			menuContainer: "#jeodpp-menu-container", // ID of the container that will hold the main menu - if not declared it defaults to "#jeodpp-menu-container"
			sliderContainer: "#jeodpp-menu-container" // ID of the container that will hold the sliders for dates and opacity - if not declared it defaults to "#jeodpp-menu-container"
		};
		//If you only want a simple base map in the compare map, it's easy, just use this:
		//enableJeodppMenu(map, jeodppOptions);
		
		//if you want to do all kinds of stuff you use this (I wrap it in ajax as it's an easily readable way to use a js promise)
		$.ajax({
			url: enableJeodppMenu(map, jeodppOptions),
			success:function(){
				// IMPORTANT -- The compare map is a global variable called jeodppAfterMap
				// Here is an example for adding additional layers to the after map.
				jeodppAfterMap.on('load', function () {
					jeodppAfterMap.addSource("BIOPAMA_Poly", {
						"type": 'vector',
						"tiles": [mapPolyHostURL+"/{z}/{x}/{y}.pbf"],
						"minZoom": 0,
						"maxZoom": 18,
					});
					jeodppAfterMap.addLayer({
						"id": "wdpaAcpFill",
						"type": "fill",
						"source": "BIOPAMA_Poly",
						"source-layer": mapPaLayer,
						"minzoom": 1,
						"paint": {
							"fill-color": [
								"match",
								["get", "MARINE"],
								["1"],
								"hsla(173, 21%, 51%, 0.2)",
								"hsla(87, 47%, 53%, 0.2)"
							],
							"fill-opacity": [
								"interpolate",
								["exponential", 1],
								["zoom"],
								3,
								0.3,
								5,
								0.5,
								6,
								1
							]
						}
					}, 'state-label-lg');
					jeodppAfterMap.addLayer({
						"id": "wdpaAcpHover",
						"type": "line",
						"source": "BIOPAMA_Poly",
						"minzoom": 4,
						"source-layer": mapPaLayer,
						"paint": {
							"line-color": "#8fc04f",
							"line-width": 1,
							"line-opacity": [
								"interpolate",
								["exponential", 1],
								["zoom"],
								3,
								0.3,
								5,
								0.5,
								6,
								1
							]
						}
					}, 'state-label-lg');
				});
			}
		})
	});
});